<?php
define('INTELLECTMONEY_PATH', "../../../../../assets/snippets/shopkeeper/payment/intellectmoney/");
define('MODX_MANAGER_PATH', "../../../../../manager/");
define('MODX_API_MODE', true);

require_once(MODX_MANAGER_PATH . 'includes/config.inc.php');
require_once(MODX_MANAGER_PATH . 'includes/protect.inc.php');
require_once(MODX_MANAGER_PATH . 'includes/document.parser.class.inc.php');
require_once(MODX_MANAGER_PATH . "includes/controls/class.phpmailer.php");


$modx = new DocumentParser;
$modx->db->connect();
$modx->getSettings();

//System configuration
$site_name = $modx->config['site_name'];
$site_url = "http://".$_SERVER['HTTP_HOST']."/";//$modx->config['site_url'];
$dbname = $modx->db->config['dbase'];
$manager_language = $modx->config['manager_language'];
$charset = $modx->config['modx_charset'];
$dbname = $modx->db->config['dbase'];
$tb_prefix = $modx->db->config['table_prefix'];

$mod_tab_items = $tb_prefix."intellectmoney_items";
$mod_tab_payments = $tb_prefix."intellectmoney_payments";
$SHK_mod_table = $tb_prefix."manager_shopkeeper";
$SHK_mod_config_table = $tb_prefix."manager_shopkeeper_config";

$currencyArray = array(
  "rub" => array('RUB','рубли','руб.','р.'),
  "tst" => array('TST','tst','tst.','тестовая валюта')
);

function recursive_array_search($needle,$haystack){
  foreach($haystack as $key => $value){
    $current_key = $key;
    if($needle === $value || (is_array($value) && recursive_array_search($needle,$value) !== false)){
      return $current_key;
    }
  }
  return false;
}

function savePayment($dbname, $mod_tab_payments, $mod_tab_items, $SHK_mod_table, $ord_data=array()){
  global $modx;
  if(count($ord_data)==0) return false;
  extract($ord_data, EXTR_OVERWRITE);
  //update payment record
  $query = "UPDATE $mod_tab_payments SET state='$state'";
  if(!empty($trans_no)) $query .= ", sys_trans_no = '$trans_no'";
  if(!empty($payer_purse)) $query .= ", payer_purse = '$payer_purse'";
  if(!empty($payer_id)) $query .= ", payer_id = '$payer_id'";
  $query .= " WHERE orderid = '$payment_no'";
  $result = $modx->db->query($query);
  $result2 = $modx->db->update(array('paid'=>$paid), $mod_tab_items, "content = '$item_id'");
  if(!empty($status)){
    //change order status
    if (mysql_num_rows(mysql_query("show tables from $dbname like '$SHK_mod_table'"))>0){
      $change_status = $modx->db->update(array("status" => $status), $SHK_mod_table, "id = '$item_id'");
      
      $evtOut = $modx->invokeEvent('OnSHKChangeStatus',array('order_id'=>$item_id,'status'=>$status));
    }
  }
  
}


if(file_exists(INTELLECTMONEY_PATH."lang/$manager_language.php")){
  require_once (INTELLECTMONEY_PATH."lang/$manager_language.php");
}elseif(file_exists(INTELLECTMONEY_PATH."lang/russian-$charset.php")){
  require_once (INTELLECTMONEY_PATH."lang/russian-$charset.php");
}else{
  require_once (INTELLECTMONEY_PATH."lang/russian.php");
}

if(!get_magic_quotes_gpc() && isset($_REQUEST)){
  function addslashes_to_array($value){
    return addslashes($value);
  };
  $_REQUEST = array_map('addslashes_to_array', $_REQUEST);
}


function sendMailToAdmin($subject,$email,$body){
  global $modx;
  $mail = new PHPMailer();
  $mail->IsMail();
  $mail->IsHTML(false);
  $mail->From	= $modx->config['emailsender'];
  $mail->FromName	= $_SERVER['SERVER_NAME'];
  $mail->Subject	= $subject;
  $mail->Body	= $body;
  $mail->AddAddress($email);
  if(!$mail->send()){
    //echo $mail->ErrorInfo;
  }
};

//define payment system
$pay_method = "intellectmoney";

function from_request($name)
{
	global $modx;
	return isset($_REQUEST[$name]) ? $modx->db->escape($_REQUEST[$name]) : '';
}

switch ($pay_method){

####################################################
# Intellectmoney Merchant
####################################################
  case "intellectmoney":
    
    require_once (INTELLECTMONEY_PATH."intellectmoney/config.php");
    
    //get parameters
	//echo '<pre>'; print_r($_REQUEST);
    list($eshopId, $orderId, $serviceName, $eshopAccount, $recipientAmount, $recipientCurrency,
		$paymentStatus, $userName, $userEmail, $paymentData, $secretKey, $hash) =
	array(
		from_request('eshopId'), from_request('orderId'), from_request('serviceName'), from_request('eshopAccount'), 
		from_request('recipientAmount'), from_request('recipientCurrency'), from_request('paymentStatus'), from_request('userName'), 
		from_request('userEmail'), from_request('paymentData'), from_request('secretKey'), from_request('hash')
	);
	$order_secretKey = $purse['secretKey'];
	$control_hash_str = implode('::', array(
		$eshopId, $orderId, $serviceName,
		$eshopAccount, $recipientAmount, $recipientCurrency, $paymentStatus,
		$userName, $userEmail, $paymentData, $order_secretKey,
	));
	$control_hash = md5($control_hash_str);
	$control_hash_utf8 = md5(iconv('windows-1251', 'utf-8', $control_hash_str));
    
	if (($hash != $control_hash && $hash != $control_hash_utf8) || !$hash) {
		echo "Control hash win-1251: $control_hash;\nControl hash utf-8: $control_hash_utf8;\nhash: $hash;\n\n";
		exit();
	}	
	
    //Prerequest
    if($paymentStatus == 3){
        
        $query_where = "pitem.payid = ppay.id "
        ."AND pitem.content = '$orderId' "
        ."AND pitem.content = ppay.orderid "
        ."AND (ppay.payer_purse = '$recipientAmount' or (ppay.payer_purse is null and pitem.price = '$recipientAmount')) "
        ."AND pitem.unit='$recipientCurrency' "
        ."AND pitem.state='Y' "
        //."AND pitem.paid='N' "
        //."AND pitem.reserved IS NULL "
        //."AND ppay.state='I' "
		;
        
        $item_res = $modx->db->select("pitem.id, pitem.price, pitem.unit", "$mod_tab_items pitem, $mod_tab_payments ppay", $query_where);
		
		$item_res = $modx->db->query("
		   select pitem.id, pitem.price, pitem.unit FROM $mod_tab_items pitem
		   inner join $mod_tab_payments ppay on pitem.payid = ppay.id
		   inner join $SHK_mod_table mitem on mitem.id = ppay.orderid
		   where
		   pitem.content = '$orderId'
		   and (ppay.payer_purse = $recipientAmount or (ppay.payer_purse is null and pitem.price = $recipientAmount) or mitem.price = $recipientAmount)
		   and pitem.unit = '$recipientCurrency' 
		   and pitem.state = 'Y'
		 ");		
		
		//print_r($modx->fetchRow($item_res));die;
        if($modx->db->getRecordCount($item_res) >= 1){
          
          $result1 = $modx->db->update(array('state'=>'R'), $mod_tab_payments, "id='$payment_no'");
          $query2 = "UPDATE $mod_tab_items SET reserved = NOW() WHERE content = '$orderId'";
          $result2 = $modx->db->query($query2);
          
          if($modx->db->getAffectedRows()){
			$ord_data = array(
			"status" => 2,//счет выставлен
			"paid" => 'N',
			"state" => 'R',
			"trans_date" => $paymentData,
			"sys_no" => from_request('paymentId'),
			"trans_no" => from_request('paymentId'),
			"payer_purse" => $recipientAmount,
			"payer_id" => from_request('paymentId'),
			"item_id" => $orderId,
			"payment_value" => $recipientAmount,
			"payment_no" => $orderId
			);

			savePayment($dbname, $mod_tab_payments, $mod_tab_items, $SHK_mod_table, $ord_data);		  
            echo "OK";
          }else{
            echo "Order not exist inner";
            exit();
          }
        }else{
          echo "Order not exist outer";
          exit();
        }
      
      exit();
    
    //Notification
    }
	if($paymentStatus == 5){
        
        $query_where = "pitem.payid = ppay.id "
        ."AND pitem.content = '$orderId' "
        ."AND pitem.content = ppay.orderid "
        ."AND (ppay.payer_purse = '$recipientAmount' or (ppay.payer_purse is null and pitem.price = '$recipientAmount')) "
        ."AND pitem.unit='$recipientCurrency' "
        ."AND pitem.state='Y' "
        //."AND pitem.paid='N' "
        //."AND pitem.reserved < NOW() "
        //."AND ppay.state='R' "
		;
        
        $item_res = $modx->db->select("pitem.id, pitem.price, pitem.unit", "$mod_tab_items pitem, $mod_tab_payments ppay", $query_where);
		//print_r($modx->db->getRow($item_res));
        if($modx->db->getRecordCount($item_res) >= 1){
        	
      	  $ord_data = array(
			"status" => 6,//оплата полученна
			"paid" => 'Y',
			"state" => 'S',
            "trans_date" => $paymentData,
            "sys_no" => from_request('paymentId'),
            "trans_no" => from_request('paymentId'),
            "payer_purse" => $recipientAmount,
            "payer_id" => from_request('paymentId'),
            "item_id" => $orderId,
            "payment_value" => $recipientAmount,
            "payment_no" => $orderId
          );
          
          savePayment($dbname, $mod_tab_payments, $mod_tab_items, $SHK_mod_table, $ord_data);
		  echo "OK";
          
    	  }
		else{
          echo "Order not exist inner";
          exit();
        }
      
      exit();
      
    }
  
  break;
####################################################
# Null
####################################################
  default:
	
	break;
}


?>