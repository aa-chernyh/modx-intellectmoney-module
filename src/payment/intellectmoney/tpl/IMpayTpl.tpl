
[+email_string+]
<br /><br />
После нажатия на кнопку &laquo;Продолжить&raquo; вы перейдёте на интернет-сайт сервиса IntellectMoney, где и продолжите оплату. После завершения платежа товар вам будет отправлен выбранным способом доставки.<br />
Убедитесь, что на вашем <b>счете</b> имеется сумма в размере <b>[+pay_summ+] [+pay_purse_type+]</b>.
<br /><br />

<form method="post" accept-charset="cp1251" action="[+action+]">
  [+h_inputs+]
  <input type="submit" name="submit" value="Продолжить">
</form>

<br />
<a href="javascript:history.back()">&laquo; Назад</a>
