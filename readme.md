#Модуль оплаты платежной системы IntellectMoney для CMS ModX

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/display/TECH/ModX#whatnew.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/display/TECH/ModX#55773554c1862e7a324acab628e8ced8f45138
